FROM python:3.9-slim-bullseye

# Env variables that configure Python to run in a container:
# do not keep dependencies downloaded by "pip install"
ENV PIP_NO_CACHE_DIR 1
# do not write "*.pyc" files
ENV PYTHONDONTWRITEBYTECODE 1
# do not buffer input/output operations, displaying prints and log messages immediately
ENV PYTHONUNBUFFERED 1

# Install wget-*.sh dependencies
RUN apt-get -y update && apt-get install -y wget

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

ENTRYPOINT python edh.py


