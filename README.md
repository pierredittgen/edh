# Esgf Download Helper

**E**sgf **D**ownload **H**elper aka `edh` is designed to ease and add reliability to ESGF data downloads.

`edh` uses `wget-*.sh` bash scripts previously downloaded from [ESGF reference website](https://esgf.llnl.gov/index.html), to run them in background and stores the download files in target dir (one subdir by bash script)

## Settings

Before using `edh`, two steps have to be completed:

- store your credentials (openid url and password) in a configuration file
- create a dir to store the esgf bash download scripts

### Credentials file

Credentials file has to be named `.env` and contain your login/password information to esgf download site. Using a file to store credentials avoid to (re-)type this information on each run.

To create your `.env` file, copy provided [.env.example file](.env.example) template and adapts its content to your needs.

### Scripts dir

All esgf bash scripts have to be stored in a directory to be taken in account by `edh`. This is a way to prevent security issues, `edh` will only authorized to run bash scripts located in this dir.

Create a new `scripts` dir (outside of project path) and put esgf download bash scripts you want to run into this directory.

## Usage

### Using Python

If you have Python (version >= 3.8) up and running, you can just type:

```
dotenv run python edh.py -h
```

to display usage notice on how to use `edh`

Example use:

```bash
dotenv run python edh.py /path/to/scripts /where/to/write/downloaded/files esgf_name [ esgf_name\* ]
```

Note : esgf_name identifies bash download script to use. E.g. running `edh` with `NAM22_hist_tas_datadkrzde` as name will run `wget-NAM22_hist_tas_datadkrzde.sh` script on background.

### Using docker

[Docker](https://www.docker.com/) tool can be used to run `edh` to avoid setting up a python development environment.

For development purpose, docker image can be built locally using command:

```bash
docker build -t edh:latest .
```

Otherwise, container image is automagically rebuilt on project commit using [GitLab CI](https://docs.gitlab.com/ee/ci/) and available in [project container registry](https://gitlab.com/pierredittgen/edh/container_registry/)

`edh` can be run using provided [edh_docker.sh bash script](edh_docker.sh).

usage :

```bash
./edh_docker.sh /path/to/credentials/file /path/to/scripts/dir /path/to/target/dir esgf_name [ esgf_name\* ]
```
