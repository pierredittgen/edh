#!/bin/bash
#
# Handy bash script to ease running edh using docker
#
set -euo pipefail

DOCKER_IMAGE=registry.gitlab.com/pierredittgen/edh:latest

function usage() {
    prog=`basename $0`
    echo "usage: $prog credentials_file scripts_dir target_dir esgf names..."
    echo
    echo "Please use absolute paths (starting with /) for files and dirs"
    exit 1
}

if [ $# -lt 4 ]; then
    usage
fi

CREDENTIALS_FILE=$1
SCRIPTS_DIR=$2
TARGET_DIR=$3
shift
shift
shift
ESGF_NAMES=$*

# echo "CREDENTIALS_FILE == $CREDENTIALS_FILE"
# echo "SCRIPTS_DIR == $SCRIPTS_DIR"
# echo "TARGET_DIR == $TARGET_DIR"
# echo "ESGF_NAMES == $ESGF_NAMES"

if [ ! -e $CREDENTIALS_FILE ]; then
    echo "credentials file [$CREDENTIALS_FILE] not found"
    usage
fi

if [ ! -d $SCRIPTS_DIR ]; then
    echo "scripts dir [$SCRIPTS_DIR] not found"
    usage
fi

echo docker run --env-file $CREDENTIALS_FILE \
    -v $SCRIPTS_DIR:/esgf_scripts \
    -v $TARGET_DIR:/dist \
    $DOCKER_IMAGE /esgf_scripts /dist $ESGF_NAMES