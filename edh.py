#!/usr/bin/env python
"""
Esgf Download Helper

Ease netcdf files batch downloading from ESGF web site
"""
import argparse
import logging
import os
import sys
from pathlib import Path


def esgf_script_path_from_name(script_dir_path: Path, name: str) -> Path:
    """Computes esgf bash script path from script dir path and download name."""
    return script_dir_path / f"wget-{name}.sh"


def run_script(script_path: Path, script_target_dir: Path):
    """Run download script in background.

    This is a tricky task that needs to be tested thoroughly
    on a command line, things to do are:
    - cd to target dir
    - run:
       echo {openid_passwd} | /bin/bash /path/to/wget_<name>.sh -H -o {openid_login} > <name>.out 2> <name.err>

    Use subprocess.Popen, it's normally possible to:
    - pass openid password as stdin input
    - store stdout and stderr handlers as log files
    - retrieve and store process id to further check if the download process is still running

    e.g.

    cmd = ["/bin/bash", "/path/to/scripts/wget-myname.sh", "-H", "-o", open_id_url]
    subprocess.Popen(cmd,
                     cwd=script_target_dir,
                     ...)

    """
    log.info(
        "> TODO: Run %r script to download content in %r dir",
        str(script_path),
        str(script_target_dir),
    )


def run_scripts(target_dir: Path, script_path_list: list[tuple[Path]]):
    """Run download scripts in parallel and return."""

    for name, script_path in script_path_list:
        log.info("Running %s download...", name)
        script_target_dir = target_dir / name

        log.debug("Creating script target dir %r", str(script_target_dir))
        script_target_dir.mkdir(exist_ok=True)

        log.debug("Starting %s download...", name)
        run_script(script_path, script_target_dir)


log = logging.getLogger(__name__)


def main():

    parser = argparse.ArgumentParser(usage="run and monitor Esgf downloads")
    parser.add_argument("scripts_dir", type=Path, help="specify esgf bash scripts dir")
    parser.add_argument(
        "target_dir", type=Path, help="specify where to save downloaded files"
    )
    parser.add_argument(
        "names",
        action="append",
        help="data names to download, e.g. NAM22_hist_pr_ipslupmcfr. Separate multiple names by space",
    )
    parser.add_argument("--log", default="INFO", help="level of logging messages")
    args = parser.parse_args()

    if args.scripts_dir is None:
        parser.error(
            "scripts dir not set. Please use --scripts-dir option or set SCRIPTS_DIR env variable"
        )

    if not args.scripts_dir.is_dir():
        parser.error("Scripts dir %r not found", str(args.scripts_dir))

    if not args.target_dir.is_dir():
        log.info("Creating target dir: ", args.target_dir)
        args.target_dir.mkdir()

    if not args.names:
        parser.error("No download names provided. Stopping here.")

    # Configure logging
    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(asctime)s:%(message)s",
        level=numeric_level,
        stream=sys.stdout,  # Use stderr if script outputs data to stdout.
    )

    # Check credentials
    user_login = os.getenv("USER_LOGIN")
    if not user_login:
        parser.error("Please define USER_LOGIN either as ENV variable or in .env file")

    user_password = os.getenv("USER_PASSWORD")
    if not user_password:
        parser.error(
            "Please define USER_PASSWORD either as ENV variable or in .env file"
        )

    # Check for download script existence
    script_path_list: list[tuple[str, Path]] = []
    for name in args.names:
        script_path = esgf_script_path_from_name(args.script_dir, name)
        if not script_path.is_file():
            parser.error("Can't find download script %r", str(script_path))
        script_path_list.append((name, script_path))

    # And really run scripts if no error occurs before
    run_scripts(args.target_dir, script_path_list)


if __name__ == "__main__":
    sys.exit(main())
